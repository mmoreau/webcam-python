#!/usr/bin/python3

import cv2
from time import time

try:
	video = cv2.VideoCapture(0)

	while True:

		start = time()
		check, frame = video.read()

		cv2.imshow("Capturing", frame)

		#25 Frames Per Second (1000 / 25 = 40)
		if cv2.waitKey(40) & 0xFF == ord("q"):
			break

		fps = video.get(cv2.CAP_PROP_FPS)
		print(f"{fps // (time() - start)} FPS")

	video.release()
	cv2.destroyAllWindows()
except:
	pass