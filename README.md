<h1>Webcam</h1>
<p>Display in real time the frames (images) of the webcam.<p>

<h2>Privacy</h2>
<p>For those who are concerned about their privacy, this script does not store or send any data.</p>

<h2>Dependencies</h2>
<pre>
    pip3 install opencv-python
</pre>

<h2>Script</h2>

<p>Key "q" to stop the script</p>

<h3><b>Windows</b></h3>
<p>Double click on the "script.py" file</p>

<h3><b>Linux</b></h3>
<pre>
    chmod +x script.py
    ./script.py
</pre>

<h3><b>Mac</b></h3>
<pre>
    python3 script.py
</pre>

<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Mail :</b> mx.moreau1@gmail.com<p>
<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>